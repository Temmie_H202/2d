﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class PlayerController : MonoBehaviour
{
    Rigidbody2D rB2D;


    public float runspeed;
    public float jumpforce;
    public TextMeshProUGUI CountText;

    private int count;


    public SpriteRenderer SpriteRenderer;
    public Animator animator;
    private Rigidbody2D rbe;


    // Start is called before the first frame update
    void Start()
    {
        rbe = GetComponent<Rigidbody2D>();
        count = 0;

        SetCountText();


        rB2D = GetComponent<Rigidbody2D>();

    }


    void SetCountText()
    {
        CountText.text = "Count: " + count.ToString();
    }


    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("PickUp"))
        {
            other.gameObject.SetActive(false);
            count = count + 1;
            SetCountText();

        }

    }


    // Update is called once per frame
    void Update()
    {
        if(Input.GetButtonDown("Jump"))
        {
           
            int levelMask = LayerMask.GetMask("Level");

            if(Physics2D.BoxCast(transform.position, new Vector2(1f, .1f), 0f, Vector2.down, .01f, levelMask))
            {
                jump();
                
            }
        }
    }


    private void FixedUpdate()
    {
        float horizontalInput = Input.GetAxis("Horizontal");

        rB2D.velocity = new Vector2(horizontalInput * runspeed * Time.deltaTime, rB2D.velocity.y);

        if(rB2D.velocity.x > 0)
        {
            SpriteRenderer.flipX = false;
        }
        else
        {
            SpriteRenderer.flipX = true;
        }

        if( Mathf.Abs(horizontalInput) > 0f)
        {
            animator.SetBool("isrunning", true);
        }
        else
        {
            animator.SetBool("isrunning", false);
        }


    }



    void jump()
    {
        rB2D.velocity = new Vector2(rB2D.velocity.x, jumpforce);
    }








}
